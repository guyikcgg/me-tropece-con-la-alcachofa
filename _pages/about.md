---
layout: page
title: Acerca de este proyecto
permalink: /about
comments: false
---

>   "Me tropecé con la alcachofa" y ya está, tío.

Esa fue la respuesta de mi amigo Javi cuando le contaba por enésima vez el _proyecto_ de crear una web con las recetas que aprendía a cocinar, cuando aún no tenía nombre y me preocupaba no elegir el correcto. Y así se quedó, no le di más vueltas. Porque... ¿por qué no? Las alcachofas son de un color muy bonito, son sabrosas y, en realidad, a penas las uso en mis recetas. Así que perfecto.

Pero, ¿qué sentido tiene publicar este recetario? En realidad, es un recetario para consulta propia. Ni pretendo ser un gran chef ni pretendo convertirme en una referencia a ningún nivel. Lo que quiero es no tener que darle demasiadas vueltas a lo que vamos a comer hoy. Por eso, la intención es recoger aquí los platos que comemos habitualmente en casa: aquellos que son más para salir del paso y se preparan en un _plis plas_, o los que son para cocinar a lo grande y congelar _táperes_ (¿se escribe así?), procurando siempre ensuciar pocos cacharros, que luego hay que fregar. Pero bueno, ya que estamos, también recojo aquellos platos más ocasionales: los que cocinas cuando te vienes arriba y quieres impresionar a tus colegas, los que llevarías a una fiesta para compartir entre todos, o aquello que cocinaste una vez, quedó riquísimo y necesitas tener las proporciones anotadas en algún lado. Por esto mismo, las recetas tienen también un formato imprimible.

Sin más pretensiones que estas, pero con _to_' el acento de mi Andalucía natal, te doy la bienvenida a este recetario.

[¿Quién está detrás de esto?](https://guyikcgg.github.io/)


###### Créditos

Esta página web es [software libre](https://www.gnu.org/philosophy/free-sw.es.html). Eres libre de usarla como quieras, estudiarla, compartirla, ¡incluso usarla como base para crear tu propio recetario y compartir tus recetas! Puedes consultar el código fuente <!--y la licencia--> en este repositorio alojado en GitLab: [https://gitlab.com/guyikcgg/me-tropece-con-la-alcachofa](https://gitlab.com/guyikcgg/me-tropece-con-la-alcachofa).

La estructura de las recetas, con pocos pasos precisos y sencillos, está inspirada en el libro _101 Recetas para Estudiantes_, de Barney Desmazery (Grijalbo, 2011).

Me encanta la sencillez y la elegancia de [El petit chef](https://elpetitchef.com/), que además de explicar los platos con pelos y señales, tiene técnicas de cocina y un curso de salsas muy útiles. Ojalá llegar al punto de incorporar técnicas o de hacer algún monográfico en algún momento.

Esta es una página web estática. No utiliza _cookies_ y respeta la privacidad de los usuarios. Está construida con [Jekyll](http://jekyllrb.com/) usando como base la plantilla [Mediumish (desarrollada por Sal para WowThemes)](https://github.com/wowthemesnet/mediumish-theme-jekyll), y se aloja en [GitLab Pages](https://docs.gitlab.com/ee/user/project/pages/).

Al igual que [Remember the Users](https://www.remembertheusers.com/about/0000-about.html):
_No cookies, ~~no JavaScript,~~ no Flash, no ads, no tracking. Just a blog._
