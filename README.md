# Me Tropecé con la Alcachofa

Un recetario con acento y aires de libertad. ¿Quieres saber más sobre su historia y pretensiones? ¡No te pierdas lo que está por venir!

[¿Quién está detrás de esto?](https://guyikcgg.github.io/)


###### Créditos

Esta página web es [software libre](https://www.gnu.org/philosophy/free-sw.es.html). Eres libre de usarla como quieras, estudiarla, compartirla, ¡incluso usarla como base para crear tu propio recetario y compartir tus recetas! Puedes consultar el código fuente y la licencia en este repositorio alojado en GitLab: [https://gitlab.com/guyikcgg/me-tropece-con-la-alcachofa](https://gitlab.com/guyikcgg/me-tropece-con-la-alcachofa).

Esta es una página web estática está construida con [Jekyll](http://jekyllrb.com/) usando como base la plantilla [Mediumish (desarrollada por Sal para WowThemes)](https://github.com/wowthemesnet/mediumish-theme-jekyll), y se aloja en [GitLab Pages](https://docs.gitlab.com/ee/user/project/pages/).