function updateTooltipText(e, newText) {
    var tooltip = e.querySelector('span.custom-tooltip-text');

    tooltip.setAttribute('data-tooltip-text', tooltip.innerHTML);
    tooltip.innerHTML = newText;
}

function renderTooltipText(e) {
    var tooltip = e.querySelector('span.custom-tooltip-text');

    originalText = tooltip.getAttribute('data-tooltip-text');
    tooltip.innerHTML = originalText;
}