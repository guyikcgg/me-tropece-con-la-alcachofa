---
layout: post
title:  "Arroz con leche"
author: cristian
categories: [Postres que quitan el sentío]
tags: [Sin gluten, Vegetariano]
image: assets/images/arroz-con-leche-01.jpg
featured: false
time: 40 minutos, más el tiempo de refrigeración
servings: 8-10 cacitos
---

Tradicional y suculento arroz con leche que no necesita más presentación.


#### Ingredientes

* 1 litro de leche de vaca (entera o semidesnatada)
* 1/2 vaso de arroz redondo
* Un trozo de canela en rama
* La cáscara de medio limón
* 7 cucharadas soperas rasas de azúcar
* Un poco de canela molida


#### Preparación

1. Poner la leche, la cáscara de limón y la canela en rama en una olla y calentar. Antes de que la leche empiece a hervir, añadir el arroz. Remover durante 20 minutos aproximadamente.
2. Añadir el azúcar y remover durante 5 minutos más.
3. Apagar el fuego y repartir la mezcla en cacitos para flanes o cualquier otro recipiente para postre. Poner un poco de canela molida en cada cuenco y, una vez enfriado, estará listo para comer.
