---
layout: post
title:  "Dorada al horno con verduras"
author: karen
categories: [Cenas ligeras]
tags: [Sin gluten]
image: assets/images/dorada-al-horno-con-verduras-01.jpg
featured: false
time: 1 hora
servings: 2 raciones
---

Deliciosa y genuina dorada <br>
sobre una cama de verdura caramelizada.


#### Ingredientes (para 2 personas)

* 1 dorada grande entera (desescamada y sin tripas)
* 2 cebolletas cortadas en juliana
* 2 tomates cortados en rodajas
* 2 pimientos italianos verdes grandes enteros
* 2 ramas de romero fresco
* 2 rodajas de limón partidas en medias lunas
* Un buen chorreón de aceite de oliva virgen extra
* Sal al gusto


#### Preparación

1. Precalentar el horno a 220 ºC y engrasar una bandeja para horno con aceite de oliva.
2. En esta bandeja, poner las distintas verduras una al lado de la otra sin que se mezclen, salarlas al gusto y hornear durante 30 minutos a 220 ºC.
3. Mientras la verdura se cocina, practicar dos hendiduras diagonales en cada lado del pescado e insertar las medias lunas de limón. Pasados los 30 minutos, colocar la dorada y las dos ramas de romero sobre las verduras y hornear durante 20 minutos más a 180 ºC.
