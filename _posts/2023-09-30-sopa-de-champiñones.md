---
layout: post
title:  Sopa de champiñones
author: karen
categories: [Pa' calentarse el cuerpo]
tags: [Sin gluten, Bajo en histamina, Vegano]
#image: assets/images/nombre-del-plato-01.jpg
featured: false
time: 45 minutos
servings: 4 raciones
---

Con esta receta quiero darle comienzo al otoño y la época de resfriados. Se trata de una receta sencilla, económica y muy sabrosa. La hice por primera vez este otoño porque una personita muy resfriada y con carita convalenciente me dijo: "cualquier cosa que cocines estará genial, pero... estaría taaaan bien una sopa de champiñones" y como la comida casera lo cura todo (o eso dicen las abuelas) hice esta receta un poco a mi gusto para calentar el cuerpo en esos días de lluvia.

#### Ingredientes

* 350 - 500 g de champiñones cortados en dados
* 1 cebolleta cortada en dados
* 3 dientes de ajo picado
* 1 litro de caldo de pollo o verduras (a elegir)
* 2 cucharadas de perejil fresco picado
* 3 cucharadas de aceite de oliva
* 1 cucharada de maicena (opcional)


#### Preparación

1. Pochamos la cebolleta y los ajos en una olla con el aceite. Cuando estén hechos echamos los champiñones y cocinamos a fuego lento 10 minutos. Salpimentamos.
2. Agregamos el caldo y el perejil y cocemos a fuego lento 20 minutos.
3. Para tener un caldo más consistente, cogemos un par de cucharadas de champiñones y caldo que estamos cocinando y lo trituramos con una cucharada de maicena. Mezclamos todo en la olla y hervimos durante 2 minutos más.


#### Referencias

Receta inspirada en https://recetasdecocina.elmundo.es/2013/05/sopa-champinones-receta-facil.html.