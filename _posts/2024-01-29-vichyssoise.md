---
layout: post
title:  Vichyssoise
author: karen
categories: [Cenas ligeras]
tags: [Sin gluten]
image: assets/images/vichyssoise-01.jpg
featured: false
time: 35 minutos
servings: 4-6 raciones
---

Una crema deliciosa, suave y versátil, que puede tomarse caliente o fría. Ideal para cualquier época del año.


#### Ingredientes

* 3 puerros troceados
* 2 patatas medianas, troceadas
* 1 L de caldo de pollo (también puede ser vegetal)
* 200 mL de nata para cocinar


#### Preparación

1. En una olla que no se pegue, sofreír el puerro en AOVE a fuego medio, hasta que empiece a dorarse (unos 10 minutos).
2. Añadir las patatas y el caldo de pollo. Llevar a ebullición y cocinar a fuego medio-lento durante 15 minutos más, o hasta que la patata esté tierna.
3. Triturar y agregar la nata para cocinar. Una vez que hierva, estará listo para servir. Si es necesario, rectificar la sal.
4. Servir caliente o muy frío.


#### Notas

Si lo sirves caliente, queda muy bien con unos picatostes (o unos champiñones salteados con perejil si quieres echarle fantasía).
