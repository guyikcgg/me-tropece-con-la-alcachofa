---
layout: post
title:  Omelet de champiñones
author: cristian
categories: [Cenas ligeras]
tags: [Vegetariano]
image: assets/images/omelet-de-champiñones-01.jpg
featured: true
time: 15 minutos
servings: 1 ración
---

Tortilla _gourmet_ y muy vistosa con la que puedes solucionarte una cena en un santiamén.


#### Ingredientes

* 2 huevos
* 1 cucharada sopera de harina (puede ser harina fina de maíz)
* Unos cuantos champiñones troceados (6 o 10 si son pequeños)
* Un poco de queso rallado
* Aceite de oliva
* Perejil y sal

#### Preparación

1. En una sartén amplia, freímos los champiñones a fuego medio con un chorreoncito de aceite de oliva. Añadimos una pizca de sal y un poco de perejil y reservamos en un plato aparte.
2. Mientras se cocinan los champiñones, batimos los huevos junto a la harina y una pizca de sal. Podemos hacerlo en un cuenco, ayudándonos de un tenedor o de unas varillas para evitar la aparición de grumos. Una vez retirados los champiñones, vertemos los huevos batidos en la misma sartén, bajamos el fuego y tapamos.
3. Pasados 2 o 3 minutos, cuando la parte superior aún está a medio hacer, colocamos en una mitad del omelet el queso rallado y los champiñones que teníamos reservados. Cerramos el omelet con cuidado y tapamos para que termine de cocinarse el huevo y se funda el queso.


#### Notas

En el omelet de la foto añadimos una loncha de jamón porque nos quedaban muy pocos champiñones. Puedes añadir cualquier otro ingrediente, ¡imaginación al poder!
