---
layout: post
title:  "Crema de calabaza"
author: cristian
categories: [Pa' calentarse el cuerpo]
tags: [Sin gluten, Vegano]
image: assets/images/crema-de-calabaza-01.jpg
featured: false
time: 35 minutos
servings: 4-5 raciones
---

Una crema ideal para calentarse el cuerpo en otoño. La cebolleta le confiere un sabor que ni las pastillas de caldo.


#### Ingredientes

* 1 calabaza pequeña
* 3 patatas medianas
* 1 cebolleta
* 2 zanahorias
* Un chorreón de aceite de oliva virgen extra (como un cucharón)
* Sal al gusto
* Un toque de pimienta o tomillo
* Pipas de calabaza (opcional)


#### Preparación

1. Poner todas las verduras peladas y cortadas en trozos grandes en una olla y cubrir con agua. Añadir un buen chorreón de aceite de oliva y cocer a fuego medio durante unos 25 minutos.
2. Una vez que la verdura esté bien tierna, triturar hasta conseguir una crema homogénea y salar al gusto.
3. Servir con un toque de pimienta o tomillo, o espolvoreando unas pipas de calabaza por lo alto.

**Consejo**: es preferible quedarse un poco cortos de agua y tapar la olla, ya que siempre se puede añadir un poco al final para ajustar la textura de la crema.
