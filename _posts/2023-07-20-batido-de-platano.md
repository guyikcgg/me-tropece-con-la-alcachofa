---
layout: post
title:  "Batido de plátano"
author: cristian
categories: [Desayunos y meriendas]
tags: [Sin gluten]
image: assets/images/batido-de-platano-01.jpg
featured: true
time: 5 minutos
servings: 2 vasos
---

Refrescante batido de plátano para un momento goloso.


#### Ingredientes

* 1 plátano bien maduro
* 400 mL de leche
* Una cucharada de miel
* Una pizca de canela
* Una cucharadita de polen de abejas (opcional)


#### Preparación

1. Batir todos los ingredientes en la batidora, hasta conseguir un batido homogéneo.
2. Servir frío y consumir inmediatamente, de lo contrario el plátano se oxidará y el batido empezará a oscurecerse.


#### Notas

Puedes usar leche de vaca o alguna leche vegetal. ¡Incluso mezclarlas!

El polen de abejas, además de añadir valor nutricional al batido, le da un toque amarillo muy apetecible.
