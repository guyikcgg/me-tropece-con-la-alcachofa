---
layout: post
title:  "Crema de calabacín mejorada"
author: cristian
categories: [Cenas ligeras]
#tags:
image: assets/images/crema-de-calabacin-mejorada.jpg
featured: false
comments: false
---

Esta es la crema que más prepara mi madre, sobre todo para la cena. Ella la llama "puré de calabacín", nombre que a mí, por haber crecido con él, me parece muy apropiado. No obstante, a cierta persona le divierte este nombre ya que su consistencia le recuerda más a una crema, su color no corresponde con el verde característico del calabacín, y, cierto es, solo lleva un calabacín (grande, eso sí). A fin de evitar conflictos diplomáticos, publicaré la receta con un nombre híbrido.

Esta crema puede hacerse solo de verduras, no obstante mi madre suele ponerle unos quesitos (tipo El Caserío) o un poco de pollo para darle más sabor. A mí personalmente me gusta mucho cómo queda con el pollo, así que dejo esta versión.


#### Ingredientes

* 1 calabacín grande
* 3 patatas medianas
* 1 cebolla
* 4 zanahorias
* Un poco de pollo (yo suelo poner la carcasa)
* Un chorreón de aceite de oliva virgen extra
* Sal al gusto


#### Preparación

1. Poner todas las verduras peladas y cortadas en trozos grandes en una olla, junto con el pollo, y cubrir con agua. Añadir un chorreón de aceite de oliva (como un cucharón) y cocer a fuego medio durante unos 25 minutos.
2. Una vez que la verdura esté bien tierna, retirar el pollo para desmenuzarlo y triturarlo todo junto hasta conseguir una crema homogénea. Salar al gusto y servir caliente.


**Consejo**: es preferible quedarse un poco cortos de agua y tapar la olla, ya que siempre se puede añadir un poco al final para ajustar la textura de la crema. Si ves que te has pasado de agua, puedes retirar un poco de caldo antes de triturarlo y todo, o añadirlo poco a poco al final para conseguir la textura que más te guste.
