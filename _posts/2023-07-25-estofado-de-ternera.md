---
layout: post
title:  Estofado de ternera
author: cristian
categories: [Pa' calentarse el cuerpo]
tags: [Sin gluten, Platónico]
image: assets/images/estofado-de-ternera-01.jpg
featured: false
time: 50 minutos
servings: 4 raciones
---

Este guiso de ternera es ideal para calentarse el cuerpo en cualquier momento del año. No solo es nutritivo y está riquísimo, sino que emplearemos un pequeño truco para simplificar su elaboración: pondremos muchas de las verduras enteras y las trituraremos cuando estén cocinadas, añadiendo el triturado de nuevo a la olla como último paso. Así, espesaremos el caldo y potenciaremos su sabor, además de ahorrarnos un tiempo precioso.


#### Ingredientes

![Foto de los ingredientes](../assets/images/estofado-de-ternera-ingredientes-01.jpg)

* 400g de carne ternera troceada
* 8 patatas pequeñas (alrededor de 400g), peladas y cortadas por la mitad
* 1 tomate, entero
* 1 pimiento verde pequeño, entero
* 1 cebolla, entera pelada
* 2 zanahorias, enteras peladas
* 2 zanahorias, cortadas en rodajas
* 1 hoja de laurel
* 1 cucharón de aceite de oliva
* 1 cucharadita de cúrcuma (opcional, también puedes cambiarla por un poco de azafrán o de colorante)
* 2 cucharadas soperas rasas de sal
* Pimienta al gusto (opcional)


#### Preparación

![Foto de los ingredientes en la olla](../assets/images/estofado-de-ternera-elaboracion-01.jpg)

1. Poner todos los ingredientes en una olla amplia y cubrir con agua (3 o 4 tazas). Llevar a ebullición y seguir cocinando con la olla tapada a fuego medio-alto durante 25 minutos.
2. Pasado este tiempo, retirar las verduras que pusimos enteras (el tomate, el pimiento, la cebolla y dos de las zanahorias) y triturarlas en el vaso de la batidora. Añadir las verduras trituradas de nuevo a la olla, remover y rectificar de sal.
3. Servir caliente en un plato hondo o en un cuenco.


#### Notas

Puedes añadir un par de dientes de ajo, guisantes o algunos corazones de alcachofa troceados, que le darán mucho sabor al plato.
