---
layout: post
title:  "Fajitas de pollo suaves"
author: cristian
categories: [Cenas ligeras]
tags: [De importación]
image: assets/images/fajitas-de-pollo-suaves-01.jpg
featured: false
time: 25 minutos
servings: 4 fajitas (2 raciones)
---

Delicionsas fajitas para tener la cena en un santiamén.


#### Ingredientes

* 1/2 pechuga de pollo, cortada en tiras
* 2 pimientos verdes italianos, cortados en tiras
* 1 cebolla blanca, cortada en tiras
* 4 lonchas de queso semicurado
* Aceite de oliva virgen extra
* Un toque de pimentón y de especias morunas
* Sal al gusto


#### Preparación

1. Sazonar el pollo con las especias y la sal. Cocinar en la sartén con un poco de aceite a fuego medio-rápido y reservar (lo ideal es que quede un poco dorado pero sin llegar a secarse).
2. Saltear el pimiento y la cebolla en la misma sartén.
3. Una vez listo el relleno, montar las fajitas, poniendo una loncha de queso semicurado, un poco de verduras y un poco de pollo. Sellar en la sartén o en una plancha para que la tortilla se dore.