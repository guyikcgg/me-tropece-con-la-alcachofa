---
layout: post
title:  "Torta de aceite y frutos secos"
author: cristian
categories: [Desayunos y meriendas]
tags: [Fuera de carta]
image: assets/images/torta-de-aceite-y-frutos-secos-01.jpg
featured: false
time: 35 minutos
servings: 8 porciones
---

Esta tradicional torta de aceite destaca por la sencillez en su elaboración y por su consistencia. Además, los frutos secos le dan un toque de sabor que enriquece la sobriedad que la caracteriza.


#### Ingredientes

* 2 vasos (a ras) de harina de trigo
* 1 vaso de azúcar
* 1 cucharada de semillas de anís (matalahúva)
* 0.8 vasos de aceite de oliva virgen
* 1 vaso de leche de vaca
* 1 sobre de levadura química Royal
* 1 vaso aprox. de frutos secos variados (a elegir dos o tres entre nueces, almendras, anacardos, uvas pasas, avellanas...)
* Algunos frutos secos más para decorar


#### Preparación

1. Precalentar el horno a 180 ºC.
2. En un cuenco hondo, poner todos los ingredientes salvo los frutos secos y mezclar bien con la ayuda de un tenedor. Cuando se consiga una masa homogénea, añadir los frutos secos y volver a mezclar.
3. Colocar una lámina de papel vegetal sobre la bandeja del horno y verter la masa sobre el mismo. La masa debería ser lo suficientemente consistente como para no requerir molde. Adornar por encima con más frutos secos.
4. Hornear a 180ºC durante 25 minutos. Transcurrido este tiempo, la torta empezará a dorarse y estará cocinada por dentro (podemos comprobarlo introduciendo un cuchillo y verificando que sale limpio).

![Foto de la torta recién sacada del horno](../assets/images/torta-de-aceite-y-frutos-secos-02.jpg)
