---
layout: post
title:  Tayín de frutos secos
author: cristian
categories: [A fuego lento]
tags: [Vegano, Sin gluten]
image: assets/images/tayin-de-frutos-secos-01.jpg
featured: false
time: 30 minutos de preparación, más 1 hora y media de coción (2 horas en total)
servings: 6-8 raciones
---

Durante una buena temporada, cierto amigo hacía este tayín casi todos los fines de semana y la casa se impregnaba de un irresistible olor a comino que lo inundaba todo. Se trata de un plato originario del norte de áfrica que tradicionalmente se prepara en un recipiente de barro de tapa cónica muy característico, y suele llevar algo de carne o de pescado. Muy característico también es que no se remueve durante la cocción, haciendo que se tuesten los ingredientes del fondo. Nosotros preparamos esta versión vegana en una olla alta, poniendo los ingredientes por capas sin cocinarlos previamente. Queda súper rico y es muy fácil de elaborar (sólo hay que tener un poco de paciencia con la cocción). Y como lleva boniato y frutos secos, este plato se ha convertido en un clásico del otoño.


#### Ingredientes

* 2 cebollas grandes (400 g), cortadas en juliana
* 4 patatas medianas (1 kg), cortadas en rodajas finas (de 1 cm aproximadamente)
* 100 g de nueces
* 100 g de anacardos
* 200 g de ciruelas pasas sin hueso
* Un puñado de uvas pasas y de pistachos (opcional)
* 2 boniatos (1 kg), cortadas en rodajas finas
* 3 manzanas grandes (600 g), cortadas en rodajas
* Comino en grano (unos 10-20 g) y sal


#### Preparación

1. En una olla grande, echamos un chorreón de aceite de oliva y movemos para que todo el fondo se impregne. A continuación, colocaremos los ingredientes por capas, poniendo comino en grano y sal entre capa y capa, en este orden: cebolla al fondo, luego patatas, luego los frutos secos y las ciruelas secas, luego batatas y finalmente, arriba del todo, manzanas.
2. Tapamos la olla y cocinamos a fuego muy lento durante al menos 1 hora. Cuanto más a fuego lento, menos probable será que se queme la cebolla y todo quedará más rico, pero tendremos que echarle paciencia. Sabremos que el tayín está hecho cuando la batata esté blanda. Servir caliente.


#### Notas

No escatimes con el comino, ¡es un ingrediente esencial en este plato!

Puedes cocinar este tayín en una olla de cocción lenta y dejarlo unas dos horas, asegurándote que la cebolla no se quema.

La receta original era tayín de pollo. En lugar de frutos secos, llevaba unos filetillos de pechuga. Queda muy rico también y el tiempo de cocción es idéntico. ¡También puedes probar a poner pollo y frutos secos!
 
