---
layout: post
title:  Mayonesa de leche
author: cristian
categories: [Acompañamientos]
tags: [Sin Gluten]
image: assets/images/mayonesa-de-leche.jpg
featured: false
time: 7 minutos
servings: 1 taza
---

Esta versión de mayonesa, aunque poco ortodoxa, queda riquísima y se hace en un santiamén. Os dejo la receta "en taza" porque me parece muy práctico hacerla así, pero puedes usar el vaso de la batidora si te parece más cómodo. Lo bueno de esta mayonesa es que, al ser de leche, nos evitamos los problemas con la _salmonella_ que tienen los huevos. 


#### Ingredientes

* Un poco de leche de vaca
* Medio diente de ajo (o uno entero si es muy pequeño)
* Un buen pellizco de sal
* Un vaso de aceite de girasol
* Un chorreón de aceite de oliva (opcional)
* Limón, vinagre o mostaza (opcional)


#### Preparación

1. En una taza amplia, poner un dedo de leche de vaca, el ajo, la sal, y dos dedos de aceite de girasol. Batir a potencia mínima con la batidora de mano, posándola sobre la base de la taza y subiendo poco a poco conforme el aceite se integra.
2. Añadir más aceite para espesar la mayonesa si fuera necesario, siempre muy poco a poco para evitar que la mayonesa se corte. También se puede añadir una pequeña parte de aceite de oliva para darle un sabor más ácido.
3. Cuando la mayonesa esté perfectamente homogénea, se pueden añadir unas gotitas de limón o de vinagre, incluso de mostaza, para conseguir otros sabores.


#### Notas

La mayonesa se corta cuando hay demasiado aceite para la cantidad de leche. Lo que sucede es que la emulsión no se sostiene. Se puede recuperar poniendo más leche y reiniciando el proceso, pero añadiendo la mayonesa cortada en lugar del aceite. Para este proceso necesitaremos más espacio, así que será conveniente utilizar el vaso de la batidora.

En casa ponemos un ajo entero y hacemos una especie de alioli de leche que queda genial con las papas asadas o con la carne. ¡Pruébalo!
