---
layout: post
title:  "Ceviche de camarón"
author: karen
categories: ["Fresquito, fresquito"]
tags: [Sin gluten, De importación]
image: assets/images/ceviche-de-camaron-01.jpg
featured: true
servings: 4 raciones
time: 1 hora
---

El ceviche de camarón es uno de los platos más populares de la gastronomia costeña de Ecuador, asi como el más popular de la variedad de ceviches ecuatorianos (entre los cuales podemos encontrar el de pescado, pulpo, concha u ostras entre algunos).

La carateristica principal del ceviche de Ecuador es que tiene un aspecto mas caldoso (asi que más sabroso 😋) que otros ceviches.
Es un plato refrescante que se se sirve frío y se acompaña de clifles, maiz tostado o canguil (palomitas de maiz).


#### Ingredientes

* 800 g de langostinos crudos
* 4 tomates crudos sin piel y licuados
* 2 vasos de agua
* 1 taza de jugo de limon o lima
* 1 cucharada de salsa de tomate (ketchup)
* 1 cucharadita de mostaza
* Sal al gusto


##### Encurtido

* 3 tomates crudos cortados en daditos
* 2 Cebollas moradas cortadas en juliana
* Cilantro fresco picado
* Aceite de oliva
* Sal


##### Acompañamientos

* Chifles
* Palomitas de maiz
* Patacones


#### Preparación

1. Cocemos los langostinos en una olla con los dos vasos de agua durante 10 minutos máximo. Apartamos del fuego, reservamos el caldo, pelamos los langostinos y los reservarmos.
2. Mientras se enfría el caldo, vamos a preparar el encurtido. Primeramente, lavamos la cebolla con sal y agua. A continuación, mezclamos la cebolla, el tomate y 1/3 del jugo de limón y reservamos esta "ensalada" (el encurtido) en la nevera.
3. Vamos ahora con el caldo del ceviche. Cuando se haya enfriado, colamos el agua en el que hemos cocido los langostinos y lo mezclamos con los tomates licuados, el resto del jugo de limón, la cucharada de ketchup y la cucharadita de mostaza. Salamos este caldo al gusto.
4. Añadimos al caldo los langostinos cocidos, el encurtido y el cilantro picado (si lo pones al final siempre queda más fresco). Corregimos el punto de sal y limón si fuera necesario, y servimos con acompañamiento.


#### Notas

A rasgos generales, el plato consiste normalmente en pescado o marisco crudo que se _cocina_ con la acidez del limón o la lima. Por eso esta receta es ideal para aquellos que prueban por primera vez el ceviche o no puedan comer marisco o pescado crudo.

En la receta original los langostinos se pelan y se les quita la cabeza cuando están crudos, se hace un caldo solo con las cascaras, se cuela el caldo y luego se cuecen los lantoginos pelados en ese caldo. Sin embargo, en esta versión nos saltamos ese paso para hacerlo más sencillo y rápido.


#### Referencias

Para escribir esta receta nos hemos basado en:

* La [receta de ceviche de camarón del blog de Laylita](https://www.laylita.com/recetas/receta-ceviche-de-camaron/), en el que tiene, además, mucha información sobre la gastronomía ecuatoriana.
* La [videoreceta de ceviche de camarón de KWA en YouTube](https://www.youtube.com/watch?v=s8Fmezppr8g), que hacen vídeos chulísimos sobre comida ecuatoriana.
