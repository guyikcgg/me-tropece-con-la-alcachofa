---
layout: post
title:  "Paella de langostinos"
author: karen
categories: [A fuego lento]
tags: [Platos principales, Sin gluten]
image: assets/images/paella-de-langostinos-01.jpg
featured: true
time: 1 hora
servings: 3-4 raciones
---

Un arroz sencillo de preparar y muy sabroso. Perfecto para quedar bien con la familia el domingo sin complicarse mucho (si es que no lleva na!). Además, poniendo unas buenas raciones, se puede servir como plato único (_platónico_).


#### Ingredientes

* 1 pimiento rojo, cortado en tiras
* 1 pimiento verde, cortado en daditos
* 1 cabeza de ajos, finamente picados
* 2 tomates triturados
* 1,5 vasos de arroz
* 2,5 vasos de caldo (el que sea)
* 300 g de langostinos crudos
* 1 cucharadita de cúrcuma
* Medio vaso de aceite de oliva virgen extra


#### Preparación

1. En una paellera, calentar el aceite y sofreír las tiras de pimiento rojo a fuego medio-alto durante 5 minutos. Pasado este tiempo, retirar el pimiento con la ayuda de una espumadera y reservarlo para después.
2. En ese mismo aceite, sofreír el ajo y el pimiento verde a fuego medio durante 2 minutos. Cuando el ajo empieza a dorarse, añadir el tomate y sofreírlo todo junto durante 5 minutos más.
3. Añadir el arroz y mezclarlo con el sofrito removiviéndolo todo en la paellera. Agregar el caldo, los langostinos y la cúrcuma y salar al gusto, mezclando y repartiendo los ingredientes en la paellera. Cuando el caldo empiece a hervir, bajar el fuego y tapar. Cocinar durante 15 minutos a fuego suave.
4. Retirar del fuego y colocar las tiras de pimiento reservadas por encima del arroz. Dejar reposar tapado durante 5 minutos para que el arroz absorba el caldo completamente.
5. Servir caliente. De forma opcional, acompañar con una rodaja de limón.


#### Notas

Si no dispones de paellera, puedes usar una sartén amplia o una olla amplia y baja.
