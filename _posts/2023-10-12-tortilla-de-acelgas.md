---
layout: post
title:  Tortilla de acelgas
author: cristian
categories: [Cenas ligeras]
tags: [Vegetariano, Sin gluten]
image: assets/images/tortilla-de-acelgas-01.jpg
featured: true
time: 30 minutos
servings: 2 raciones
---

Una deliciosa tortilla de acelgas con ajitos fritos, ¿hay que explicar algo más?


#### Ingredientes

* 400 g de acelgas troceadas y sin hebras
* 4 dientes de ajo laminados o picados
* 4 huevos
* Un chorreón de aceite de oliva virgen extra


#### Preparación

1. En una sartén, ponemos un chorreón de aceite de oliva y sofreímos los ajos a fuego lento durante 2 minutos. A continuación, añadimos las acelgas y cocinamos con la tapa puesta unos 5 minutos, hasta que éstas se ablanden. Para reducir el agua que sueltan las acelgas, retiramos la tapa y cocinamos a fuego medio durante 10-15 minutos más, removiendo frecuentemente.
2. En un cuenco hondo, batimos los huevos y añadimos las acelgas que acabamos de cocinar. Removemos bien y salamos con dos buenas pizcas de sal.
3. Finalmente, calentamos un chorreoncito de aceite en una sartén antiadherente y añadimos el contenido del cuenco. Cocinamos durante 5-10 minutos con la tapa puesta a fuego medio-bajo. Pasado este tiempo, daremos la vuelta a la tortilla ayudándonos de un plato grande y cocinaremos otros 5 minutos más. Cortar en cuñas y servir caliente con un chorreoncito de limón o un poco de mayonesa.


#### Notas

Si no entra toda la acelga en la sartén, puedes ir añadiéndola poco a poco, ya que al cocinarse reduce mucho en volumen. Yo uso una única sartén pequeña y así me evito fregar dos sartenes.

Al cocinarse, las acelgas soltarán mucha agua, que tendremos que eliminar para que el huevo cuaje bien. Así que es importante seguir cocinando las acelgas hasta que reduzcan toda el agua: sin tapa, a fuego medio-alto y removiendo a menudo.

Puedes usar acelgas frescas o congeladas. También puedes sustituir las acelgas por espinacas y mantener el resto de la receta (así que, como recompensa por haber leído hasta aquí, ¡receta extra!).
