---
layout: post
title:  "Salmorejo cordobés"
author: cristian
categories: ["Fresquito, fresquito"]
tags: [Entrantes]
image: assets/images/salmorejo-cordobes-01.jpg
featured: true
time: 15 minutos, más el tiempo de refrigeración (1 hora en total)
servings: 4 raciones
---

Esta sopa fría de tomate es un clásico en la gastronomía andaluza (así que vamos a ponernos serios). Es muy típico en Córdoba, aunque en verano lo puedes encontrar casi donde sea. Es barato y fácil de hacer, ideal como entrante para refrescase en verano, y muy saciante por su consistencia.


#### Ingredientes (para 4 personas)

* 1 kg de tomates frescos (cuanto más rojos y duros, mejor)
* 200 g de pan blanco (puede ser pan del día anterior o incluso seco)
* 100 g de aceite de oliva virgen extra
* 1 diente de ajo pequeño
* 1 cucharadita de sal
* Un chorreoncito de vinagre (opcional)


##### Para poner encima

* Huevos duros picados
* Jamón serrano a taquitos


#### Preparación

1. Remojar el pan en agua fría durante unos minutos para que se ablande. Cuando esté bien hidratado, escurrir ejerciendo presión con las manos, para extraer la máxima cantidad de agua posible.
2. Con la ayuda de una batidora, triturar los tomates a máxima potencia hasta conseguir un zumo lo más líquido y homogéneo posible.
3. Añadir el resto de ingredientes y seguir batiendo a máxima potencia para que quede bien homogéneo (unos 5 minutos).
4. Refrigerar. Cuanto más frío se sirva, más rico estará.
5. Servir en un cuenco pequeño poniendo por encima el huevo duro picado, el jamoncillo y un chorreoncito de aceite de oliva.


#### Notas

Si quieres que la textura sea muy fina y sin grumos, lo ideal es pelar los tomates antes de triturarlos. Una forma fácil de pelarlos es hirviéndolos durante un par de minutos para que la piel se suelte. De forma alternativa, puedes pasar el tomate triturado por el pasapurés para eliminar los restos de piel y las semillas.

Encontrar el punto de ajo adecuado puede ser complicado. Como con la sal, es preferible empezar quedándonos cortos (poniendo medio diente, por ejemplo), e ir añadiendo trocitos pequeños hasta que consigamos el punto deseado.

Puedes jugar con la cantidad de pan para ajustar la consistencia del salmorejo hasta dejarlo a tu gusto.

En lugar de jamón, se pueden poner otros ingredientes por encima, como atún, por ejemplo.
