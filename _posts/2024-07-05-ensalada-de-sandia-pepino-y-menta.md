---
layout: post
title:  Ensalada de sandía, pepino y menta
author: karen
categories: ["Fresquito, fresquito"]
tags: [Vegetariano, Sin gluten]
image: assets/images/ensalada-de-sandia-pepino-y-menta.jpg
featured: true
time: 10 minutos
servings: 2 raciones
---

El sabor de la menta hace que esta sea una ensalada súper fresquita para los tiempos estivales. Es una buena forma de aprovechar la sandía cuando no sale muy dulce, aunque está riquísima cuando la sandía está bien dulce. 


#### Ingredientes

* 1 pepino mediano, cortado en dados pequeños
* Sandía (el doble que de pepino), cortada en dados medianos
* Media cebolla morada, cortada en dados pequeños
* Un puñadito de menta fresca (puede ser hierbabuena), laminada/picada
* Queso tipo feta desmenuzado
* Medio limón (con mucho zumo)
* Aceite de oliva


#### Preparación

1. Ponemos la cebolla en daditos en un cuenco grande y le exprimimos el medio limón. Lo dejamos reposar mientras cortamos el resto de ingredientes.
2. Añadimos el pepino y la sandía al cuenco, junto al queso y la menta. Condimentamos con sal y aceite de oliva. ¡Servimos la ensalada bien fresquita!
