---
layout: post
title:  "Perico huevo"
author: karen
categories: [Desayunos y meriendas]
tags: [De importación, Sin gluten, Vegetariano]
image: assets/images/perico-huevo-01.jpg
featured: false
time: 30 minutos
servings: 6-8 raciones
---

Este revuelto de huevo es un acompañamiento ideal para las arepas (o para poner encima de las tostadas). _Nota de importación:_ Siendo ortodoxos, deberemos tomar café de filtro con este platillo.


#### Ingredientes

* 1 cebolleta cortada en daditos (separar la parte blanca de la verde)
* 5 tomates medianos cortados en daditos
* 6 huevos de gallina
* Un chorreón de aceite de oliva
* Comino molido
* Sal al gusto


#### Preparación

1. En una sartén, poner un chorreón de aceite de oliva y sofreír la parte blanca de la cebolleta a fuego medio-bajo durante unos 5 minutos. Pasado este tiempo, añadir los tomates y cocinar completamente, procurando que no se pegue.
2. A continuación, añadir los huevos junto a la parte verde de la cebolleta y cocinarlos a fuego lento, removiendo continuamente. De esta forma, se mezclará la clara con la yema, los sabores se integrarán perfectamente y el huevo quedará jugoso.
3. Retirar del fuego cuando el huevo tenga la consistencia deseada y servir caliente.
