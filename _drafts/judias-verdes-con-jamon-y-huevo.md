---
layout: post
title:  Judías verdes con jamón y huevo
author: cristian
categories: [Cenas ligeras]
tags: []
image: assets/images/nombre-del-plato-01.jpg
featured: false
time: X minutos, más el tiempo de refrigeración (X minutos en total) # time = prep_time + cooking_time
servings: 2 raciones
---

Breve descripción del plato y reseña.


#### Ingredientes

* 250 g de judías verdes planas, cortadas en trozos
* 2 dientes de ajo picados
* 2 huevos duros
* Un puñado de taquitos de jamón
* Un chorreón de AOVE


#### Preparación

1. En una sartén, poner las judías verdes junto al ajo y un chorreón de AOVE.
2. 
3. 


#### Notas

No será necesario añadir sal a este plato, ya que el jamón serrano es bastante salado.