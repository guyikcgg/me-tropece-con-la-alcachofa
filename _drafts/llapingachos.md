---
layout: post
title:  Llapingachos (tortitas de patata)
author: autor
categories: []
tags: [De impotación, Sin gluten, Vegano]
#image: assets/images/nombre-del-plato-01.jpg
featured: false
time: X minutos, más el tiempo de refrigeración (X minutos en total) # time = prep_time + cooking_time
servings: 8 tortitas
---

Breve descripción del plato y reseña.


#### Ingredientes

* 4 patatas medianas (400g)
* 12 cucharadas colmadas de puré de patatas instantáneo
* 4 cucharadas de aceite de oliva virgen extra


#### Preparación

1. Cocer las patatas sin pelar durante 15 minutos a fuego medio-bajo, con una cucharada de sal.
2. Una vez cocidas, dejar que se enfríen las patatas y pelarlas. Con la ayuda de un tenedor, machacarlas y mezclarlas con el aceite y el concentrado de puré de patatas. Añadir también media cucharadita de sal. Está bien si quedan trocitos de patata sin deshacer, ya que le dará textura.
3. Sacar porciones y darles forma de tortita.


#### Notas

Separadas por párrafos