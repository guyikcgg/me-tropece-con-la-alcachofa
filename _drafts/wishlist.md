---
layout: post
title:  Lista de platos por registrar
author: cristian
featured: false
---

# Pescados/Entrantes

* Salmón con queso azul
* Salmón con tomate
* Salmón con limón
* Caballa/Dorada plancha
* Calamares plancha
* Calamares rellenos
* Espárragos/Alcachofas con mayonesa
* Champiñones/Setas fritas
* Habas con jamón

Salsa verde / Limón


# Pasar desde libreta

* Rosquillas de los yayos (Semana Santa)
* Sopa de marisco
* Emblanco
* Potaje de pulpo
* Pizza rica (David y Ana)
* Brownie
* Bizcocho de calabaza
* Papas viudas
* Batido de mango y naranja
* Paella
* Spaghetti de zanahoria y calabacín con salsa de tomate
* Encebollado
* Sopa de coliflor
* Tortitas americanas con arándanos y moras
* Pizza de garbanzos y pepperoni
* Muslos de pollo con verduras
* Ensalada verde
* Satsiki
* Vinagre para sushi
* Arroz para sushi


# Otros

* Empanada de dátiles
* ~~Torta de dátiles y frutos secos~~
* ~~Estofado de ternera~~
* Ensalada de pasta
* Ensaladilla rusa
* Ensaladilla cateta
* Croquetas
    * Pollo
    * Boniato
    * Jamón
    * Setas
    * Versiones sin lactosa y sin gluten
* Sopa de picadillo
* Puchero / Cocido
* Huevos rellenos
* ~~Tajin~~
* Arroz a la cubana (refrito con ajo, champiñones y atún)
* Lentejas
* Tortitas de cebolleta
* Pinchitos morunos
* Berenjenas fritas
* Empanadillas de atún
* Gazpacho
* Pollo al ajillo
* Boquerones fritos
* Caballa en adobo
* Tarta de la abuela / de cumpleaños
* Patatas a lo pobre
* Guarnición de patatas horneadas
    - Mezcladas con la cebolla y con el aliño en un bol, luego a la bandeja de hornear y listo
* Gazpachuelo
* Entremeses
* Langostinos cocidos
* Aguacates rellenos
* Canelones
* Hígados con cebolla
* Ensaladas
    * Pepino aliñao
    * Claroscuro de pepino: Pepino, salsa de soja, vinagre balsámico y sésamo negro
    * Tomate, mozzarela, albahaca
    * Ensalada mediterránea: lechuga, tomate, cebolla, maíz, huevos duros, atún (opcional: rodajas de pepino)
    * Tomate aliñao con atún/melva, ajo, aceite, una pizca de vinagre
    * Cebolla encurtida
    * Ensalada de brotes verdes, feta, balsámico, nueces, pasas?
* Bocadillos y tostadas
    * Hummus con queso crema


# Postres

* Flan de huevo
* Macedonia de frutas
* Pijama
* Mousse de limón


# Categorías

* A fuego lento
    *platos muy elaborados o que requieren de más de una hora*
* Cenas ligeras
* Pa' calentarse el cuerpo
* Triunfa en cualquier fiesta
* Desayunos y meriendas
* Postres que quitan el sentío
* "Fresquito, fresquito"
* Pa' salir del paso

## Tags
* Platónico
    *platos únicos*
* Cocina a lo grande y congela
* Sin gluten
* Vegetariano
* Vegano


* Platos exprés / Para salir del paso
