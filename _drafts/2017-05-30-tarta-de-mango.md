---
layout: post
title:  "Tarta de mango"
author: cristian
categories: [Postres que quitan el sentío]
tags: [Postres]
image: assets/images/tarta-de-mango-01.jpg
featured: false
comments: false
time: 2 horas
servings: 8 porciones
---

Esta _cheese cake_ de mango, suave y con un punto cítrico, es un postre ideal para la época de mangos. Su tiempo de preparación es de unas 2 horas, requiriendo de otras 8 horas de refrigeración una vez elaborada. Estos tiempos pueden acortarse si, en lugar del formato tarta, preparamos postres individuales introduciendo el relleno en tarritos.


#### Ingredientes

* 200g de queso de untar (tipo _Philadelphia_)
* 300g de leche condensada
* 250g de nata para montar
* 50g de azúcar glas
* 8 láminas de gelatina
* 2 cucharadas soperas de jugo de limón
* 3 mangos grandes (más de 1kg), cuanto más maduros, mejor


##### Para la base de galletas

* 200g de galletas María
* 100g de mantequilla sin sal


##### Para el sirope

* 150g de jugo de mango
* 1 cucharada sopera de azúcar
* 2 láminas de gelatina


#### Preparación

##### Base de Galletas

Moler las galletas, ayudándose de la batidora o de una bolsa limpia y un rodillo. Cuando las galletas estén perfectamente pulverizadas, mezclar con mantequilla derretida. La mantequilla deberá derretirse a fuego lento o en el microondas, usando una potencia baja.

La mezcla deberá quedar como una especie de arena, que deberá prensarse en el fondo del molde, ayudándose de una cuchara sopera.

Reservar el molde con su base en el frigorífico.


##### Relleno

Antes de comenzar, ponemos las láminas de gelatina en agua fría, para que se hidraten.

Lo primero que debemos hacer será extraer el jugo del mango puro, esto es, sin hebras. Para ello, triturar el mango en la batidora y pasar este zumo por el pasapuré.

De este zumo, reservamos 150g para el sirope, y cuatro cucharadas soperas para disolver la gelatina.

En un bol grande y frío, montamos los 250g de nata, incorporándole los 50g de azúcar pulverizada. Reservamos la nata para más tarde en la nevera.

En otro bol grande, batimos el queso de untar, junto con la leche condensada, el jugo de mango y el jugo de limón. La mezcla debe ser bastante líquida y perfectamente homogénea.

A esta mezcla hay que añadirle 8 láminas de gelatina, disueltas en el jugo de mango reservado a tal fin. Para ello, calentar a fuego lento las 8 láminas de gelatina hidratada con las 4 cucharadas de jugo de mango, sin que llegue a hervir. La mezcla deberá añadirse a la gelatina poco a poco, removiendo cada vez, para que la gelatina se estabilice sin solidificar. Cuando la disolución de gelatina ya esté fría, se incorpora al resto de la mezcla y se remueve bien.

A continuación, incorporamos la nata montada a la mezcla, con movimientos envolventes para que no se desmonte, hasta conseguir un resultado homogéneo.

Verter la nueva mezcla en el molde, y reservar en el frigorífico.


##### Sirope de Mango

En un cazo, calentar los 150g de mango que habíamos reservado, y disolver una cucharada de azúcar. Añadir las 2 láminas de gelatina reservadas (de nuevo, sin que llegue a hervir). Cuando la mezcla esté tibia, verterla sobre la tarta que tenemos en el frigorífico.



La tarta deberá permanecer en el frigorífico al menos 8 horas, para que la gelatina se cuaje bien.