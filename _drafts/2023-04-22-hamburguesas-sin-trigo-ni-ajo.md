---
layout: post
title:  "Hamburguesas sin trigo ni ajo"
author: cristian
categories: [Cena, Sin Gluten]
tags: [carne picada, huevo, cebolla, harina de arroz, perejil]
#image: assets/images/.jpg
featured: false
comments: false
---

#### Ingredientes (para 2 hamburguesas)

* 300g de carne picada (ternera y/o cerdo)
* 1 huevo batido
* Media cebolla (o cebolleta) finamente picada
* 2 cucharaditas de harina de arroz
* 6 ramitas de perejil fresco, finamente picadas
* Aceite de oliva virgen extra


#### Preparación

En un bol grande, mezclar todos los ingredientes, integrándolos bien con la ayuda de un tenedor. Machacar un poco la cebolla en el proceso.

Dar forma a las hamburguesas y cocinarlas en una sartén con un chirrito de aceite de oliva virgen extra, a fuego medio.

Si las hamburguesas no tienen una buena consistencia, agregar más harina de arroz a la masa.


#### Ideas de acompañamiento

* Patatas y boniatos cortados en gajos finos al horno, con aceite de oliva y sal
* Cebolla frita/plancha
* Huevo frito/plancha
* Ensalada de canónigos
* Queso