---
layout: post
title:  "Flan de queso"
author: cristian
categories: [Postres]
tags: [leche, huevo, quesitos, cuajada, azúcar, canela]
#image: assets/images/<img>.jpg
#description: <descripción>
comments: false
---

Esta receta me la dio mi prima Paloma. Se la pedí porque los flanes con que nos recibió en aquella ocasión estaban riquísimos. Sin embargo, nunca llegué a reproducirla. ¡Tarea para casa!


#### Ingredientes

* 1/2 litro de leche de vaca
* 2 huevos
* 8 quesitos del Caserío
* 2 sobres de preparado para cuajada Royal
* 8 cucharadas soperas rasas de azúcar
* Un poco de canela molida


#### Preparación

En un recipiente batir 1/4 de litro de leche con los quesitos, el azúcar, los huevos y el preparado para cuajada.
Poner el resto de leche en una olla y calentar.
Mientras se calienta, se tuesta un poco de azúcar. Para ello, poner unas cucharadas de azúcar en otra olla y calentar. Cuando el azúcar empiece a tomar un color miel, verterla en los recipientes donde se vaya a servir el flan.
Cuando la leche empiece a hervir, verter la mezcla batida preparado anteriormente y remover sin cesar durante unos segundos.
Verter el resultado en los recipientes en los que se va a servir y dejar enfriar. Cuando ya esté a temperatura ambiente, el flan estará listo para ser servido, si no se sirve, conservar en el frigorífico.
