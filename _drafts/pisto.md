---
layout: post
title:  Pisto
author: cristian
categories: []
tags: []
image: assets/images/pisto.jpg
featured: false
time: 15 minutos de preparación, 40 minutos de cocción
servings: 8 raciones
---

Un clásico de la cocina española, que queda ideal con un huevo frito y un poco de pan. A mí me gusta hacer pisto en grandes cantidades y congelar, de forma que luego puedo usarlo ........

#### Ingredientes

Picados en dados de 2 a 3 cm de lado:
* 2 kg de tomates
* 3 pimientos verdes
* 2 cebollas
* 2 berenjenas
* 1 calabacín grande

Además:
* 1 diente de ajo, finamente picado
* Un poco de albahaca
* 0.5 vasos de aceite de oliva
* Sal al gusto (alrededor de 1 cucharada)


#### Preparación

1. Poner los dados de berenjena en abundante agua para que suelten el amargor.
2. En una sartén grande o en una olla antiadherente, calentar el aceite de oliva y sofreír las cebollas y los pimientos durante 5-10 minutos, a fuego medio-suave y removiendo de vez en cuando. Pasado este tiempo, añadir el ajo, calabacín, y las berenjenas, previamente escurridas. Remover todo y seguir sofriendo durante otros 10 minutos.
3. Añadir el tomate, remover y tapar. Dejar cocinar durante al menos 20 minutos desde que comience a hervir, removiendo de vez en cuando. Pasado este tiempo, el tomate estará cocinado, y podremos salar al gusto. Si es necesario, dejar reducir a fuego suave.


#### Notas

En lugar de tomates frescos, puedes usar tomate triturado en lata, que se cocinará más rápido porque en general tiene que reducir menos. En este caso, necesitarás de 500 g a 1 kg de tomate en lata.