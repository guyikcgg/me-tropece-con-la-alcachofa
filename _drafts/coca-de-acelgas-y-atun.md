---
layout: post
title:  "Coca de acelgas y atún"
author: karen
categories: [para compartir]
tags: []
ingredients: [aceite de oliva, sal, azúcar, harina, cebolleta, acelga, tomate, atún]
image: assets/images/coca-de-acelgas-y-atun-01.jpg
featured: true
comments: false
time: X minutos # time = prep_time + cooking_time
servings: X raciones
---

Deliciosa coca para compartir con amigos.

#### Ingredientes

* 1 cebolleta
* 1 manojo de acelgas
* 5 tomates pera
* 1 ó 2 latas de atún
* Sal
* Aceite de oliva


##### Para la masa

* 1 taza de agua templada
* 1/3 de taza de aceite de oliva
* 1 pizca de sal
* 1 pizca de azúcar
* Harina de trigo (la que admita la masa)


#### Preparación

??

![Foto de la coca cortada](../assets/images/coca-de-acelgas-y-atun-02.jpg)