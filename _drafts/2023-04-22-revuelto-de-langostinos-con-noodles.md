---
layout: post
title:  "Revuelto de langostinos con noodles"
author: cristian
categories: [Cena, Sin Gluten]
tags: [langostinos, setas, cebolleta, noodles]
#image: assets/images/revuelto-de-langostinos-con-noodles.jpg
featured: false
comments: false
---

#### Ingredientes

* 500g de langostinos (yo prefiero comprarlos crudos y pelarlos a mano, pero también pueden estar cocidos)
* 200g de setas de ostra (_Pleurotus ostreatus_) cortadas en tiras
* 1 o 2 cebolletas picadas (separaremos la parte blanca de la verde)
* Noodles de arroz
* Un poco de aceite de oliva virgen extra


#### Preparación

1. Sofreír las cebolletas a fuego medio (solo la parte blanca)
2. Cuando la cebolleta esté medio hecha, añadir las setas y seguir sofriendo. Soltarán mucha agua que deberá evaporarse (subir fuego si es necesario).
3. A fuego medio, añadir los langostinos. Añadir también la parte verde de las cebolletas para dar color y remover bien.
4. Cocer los noodles siguiendo las indicaciones del envase.
5. Emplatar los noodles junto al revuelto.